package id.ac.ugm.customerservice;

import id.ac.ugm.customerservice.entity.Customer;
import id.ac.ugm.customerservice.repo.CustomerRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Timestamp;
import java.util.Arrays;

@SpringBootApplication
public class CustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerServiceApplication.class, args); }

    @Bean
    CommandLineRunner init(CustomerRepository customerRepository) {
        return (evt) -> Arrays.asList(
                "adi,mufti,raka,yuke,sam,andre".split(","))
                .forEach(
                        name -> {
                            @SuppressWarnings("unused")
                            Customer customer = customerRepository.save(new Customer(name, name, "", name + "@emeriocorp.com", "alamat rumah " + name,
                                    new Timestamp(System.currentTimeMillis())));
                        });
    }


}
